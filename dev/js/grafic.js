$(document).ready(function () {


    var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

    var config = {
        key1: {
            type: 'line',
            data: {
                labels: ['11 Сентября', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '5 Января','11 Сентября', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '5 Января','11 Сентября', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '11 Октября', '29 Декабря', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '1 Января', '5 Января'],
                datasets: [{
                    label: '3 месяца',
                    backgroundColor: 'red',
                    borderColor: 'red',
                    borderWidth: 0.5,
                    data: [
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,
                        125,
                        162,
                        66,
                        152,

                    ],
                    fill: false,
                },{
                    label: '1 месяц',
                    backgroundColor: 'blue',
                    borderColor: 'blue',
                    data: [
                        51,
                        55,
                        54,
                        30,
                        31,
                        32,
                        35,
                        40,
                        100,
                        0,
                    ],
                    fill: false,
                },{
                    label: 'Неделя',
                    backgroundColor: 'green',
                    borderColor: 'green',
                    data: [
                        50,
                        100,
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Наименование товара'
                },
                // tooltips: {
                //     mode: 'index',
                //     intersect: false,
                // },
                // hover: {
                //     mode: 'nearest',
                //     intersect: false
                // },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            // labelString: 'Время'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            // labelString: 'Значение'
                        }
                    }]
                }
            }

        }
    };



    window.onload = function() {
        var ctx = document.getElementById('myChart').getContext('2d');
        window.myLine = new Chart(ctx, config.key1);
    };













    document.getElementById('randomizeData').addEventListener('click', function() {
        config.data.datasets.forEach(function(dataset) {
            dataset.data = dataset.data.map(function() {
                return randomScalingFactor();
            });

        });

        window.myLine.update();
    });

    var colorNames = Object.keys(window.chartColors);
    document.getElementById('addDataset').addEventListener('click', function() {
        var colorName = colorNames[config.data.datasets.length % colorNames.length];
        var newColor = window.chartColors[colorName];
        var newDataset = {
            label: 'Dataset ' + config.data.datasets.length,
            backgroundColor: newColor,
            borderColor: newColor,
            data: [],
            fill: false
        };

        for (var index = 0; index < config.data.labels.length; ++index) {
            newDataset.data.push(randomScalingFactor());
        }

        config.data.datasets.push(newDataset);
        window.myLine.update();
    });

    document.getElementById('addData').addEventListener('click', function() {
        if (config.data.datasets.length > 0) {
            var month = MONTHS[config.data.labels.length % MONTHS.length];
            config.data.labels.push(month);

            config.data.datasets.forEach(function(dataset) {
                dataset.data.push(randomScalingFactor());
            });

            window.myLine.update();
        }
    });

    document.getElementById('removeDataset').addEventListener('click', function() {
        config.data.datasets.splice(0, 1);
        window.myLine.update();
    });

    document.getElementById('removeData').addEventListener('click', function() {
        config.data.labels.splice(-1, 1); // remove the label first

        config.data.datasets.forEach(function(dataset) {
            dataset.data.pop();
        });

        window.myLine.update();
    });



});