ymaps.ready(function () {
    var myMap = new ymaps.Map('mp-contact__map', {
            center: [52.424545, 31.004705],
            zoom: 9
        }, {
            searchControlProvider: 'yandex#search'
        }),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
            hintContent: 'Республика Беларусь, 246050, г. Гомель, ул. Катунина, 12',
            balloonContent: 'Республика Беларусь, 246050, г. Гомель, ул. Катунина, 12'
        }, {
            iconLayout: 'default#image',
            iconImageHref: './img/svg/marker.svg',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38]
        }),

        myPlacemarkWithContent = new ymaps.Placemark([53.867787, 27.523904], {
            hintContent: 'Республика Беларусь 220099 г. Минск, ул. Казинца, д. 2, к. 200',
            balloonContent: 'Республика Беларусь 220099 г. Минск, ул. Казинца, д. 2, к. 200',
        }, {
            iconLayout: 'default#imageWithContent',
            iconImageHref: './img/svg/marker.svg',
            iconImageSize: [30, 42],
            iconImageOffset: [-5, -38],
        });

    myMap.geoObjects
        .add(myPlacemark)
        .add(myPlacemarkWithContent);
    myMap.behaviors.disable(['scrollZoom', 'rightMouseButtonMagnifier']);
});