$(document).ready(function () {
	const width = $(window).width();
	const height = $(window).height();

	console.log('Width: ' + width + 'px');
	console.log('Height: ' + height + 'px');

	svg4everybody();

	let groupName = $('.inner-group__name > li'),
		groupButton = $('.inner-group__button > li');
	function toEqualHeight() {
		groupName.each(function () {
			let $this = $(this);
			groupButton.eq($(this).index()).height(function(){
				return $this.outerHeight()
			});
			if($this.hasClass('with-background')) {
				groupButton.eq($(this).index()).addClass('with-background')
				$('.inner-group__time').addClass('with-background')
			}
		})
	}
	
	toEqualHeight()

	$(window).resize(function () {
		toEqualHeight();
	});





	// groupName.hover(function () {
	// 	$(this).addClass('active')
	// 	$('.inner-group__time').addClass('active')
	// 	groupButton.eq($(this).index()).addClass('active');
	// }, function () {
	// 	groupButton.removeClass('active')
	// 	groupName.removeClass('active')
	// 	$('.inner-group__time').removeClass('active')
	// })
	// groupButton.hover(function () {
	// 	$(this).addClass('active')
	// 	$('.inner-group__time').addClass('active')
	// 	groupName.eq($(this).index()).addClass('active');
	// }, function () {
	// 	groupName.removeClass('active')
	// 	groupButton.removeClass('active')
	// 	$('.inner-group__time').removeClass('active')
	// })
	// $('.inner-group__time').hover(function () {
	// 	$(this).closest('.inner-group').addClass('active')
	// }, function () {
	// 	$(this).closest('.inner-group').removeClass('active')
	// })


	if (window.location.hash) {

		$('html, body').animate({
			scrollTop: 0
		}, 0);
		var anc;
		setTimeout(function () {
			anc = window.location.hash.replace("#", "");
			anc = $('body').find('#' + anc);
			$('html, body').animate({
				scrollTop: anc.offset().top - $('header').outerHeight(true) / 2 - 150
			}, 1000);
			window.location.hash = '';
		}, 200);
		setTimeout(function () {
			$(anc).find('.accordion-body').slideDown();
		}, 1110);
	}





	$('.partners-slider').slick({
		slidesToShow: 3,
		prevArrow: '<svg class="slick-prev" width="23" height="13" viewBox="0 0 23 13" xmlns="http://www.w3.org/2000/svg"><path d="M6.72003 11.9911C6.94485 12.2222 6.94485 12.5957 6.72003 12.8267C6.4952 13.0578 6.1318 13.0578 5.90698 12.8267L0.16905 6.9299C0.0603751 6.81998 -7.54858e-08 6.66989 -7.73602e-08 6.51271C-7.74095e-08 6.50857 0.00230209 6.50443 0.00230209 6.5003C0.00230209 6.49616 -7.75998e-08 6.49261 -7.76562e-08 6.48789C-8.00027e-08 6.29111 0.0994758 6.12565 0.243225 6.0181L5.93055 0.173289C6.15537 -0.0577619 6.51878 -0.0577619 6.7436 0.173289C6.96843 0.404339 6.96843 0.777803 6.7436 1.00885L1.9872 5.89696L22.425 5.89696C22.743 5.89696 23 6.16111 23 6.48789C23 6.81467 22.743 7.07881 22.425 7.07881L1.9389 7.07881L6.72003 11.9911Z" /></svg>',
		nextArrow: '<svg class="slick-next" width="23" height="13" viewBox="0 0 23 13" xmlns="http://www.w3.org/2000/svg"><path d="M16.28 11.9911C16.0551 12.2222 16.0551 12.5957 16.28 12.8267C16.5048 13.0578 16.8682 13.0578 17.093 12.8267L22.8309 6.9299C22.9396 6.81998 23 6.66989 23 6.51271C23 6.50857 22.9977 6.50443 22.9977 6.5003C22.9977 6.49616 23 6.49261 23 6.48789C23 6.29111 22.9005 6.12565 22.7568 6.0181L17.0695 0.173289C16.8446 -0.0577619 16.4812 -0.0577619 16.2564 0.173289C16.0316 0.404339 16.0316 0.777803 16.2564 1.00885L21.0128 5.89696L0.575 5.89696C0.257025 5.89696 8.1553e-08 6.16111 7.76562e-08 6.48789C7.37594e-08 6.81467 0.257025 7.07881 0.575 7.07881L21.0611 7.07881L16.28 11.9911Z" /></svg>',
		responsive: [
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1
				}
			}
		]
	});


	$('.main-slider__grand').slick({
		speed: 3000,
		// autoplay: true,
		fade: true,
		autoplaySpeed: 5000,
		asNavFor: '.main-slider__nav-container',
		// infinite: false,
		prevArrow: $('.main-slider__arr-prev'),
		nextArrow: $('.main-slider__arr-next')
	});


	$('.main-slider__nav-container').slick({
		speed: 2000,
		slidesToShow: 3,
		asNavFor: '.main-slider__grand',
		focusOnSelect: true,
		arrows: false,
		// infinite: false
		responsive: [
			{
				breakpoint: 450,
				settings: {
					slidesToShow: 2
				}
			}]
	});


	$('body').on('click', '.scrollTop', function () {
		$('html, body').animate({ scrollTop: 0 }, 500);
		return false;
	});

	mainSliderNavMargin(width);

	// accordion
	$('body').on('click', '.accordion-head', function () {
		$(this).siblings('.accordion-body').slideToggle();
	});


	$(document).mouseup(function (e) { // событие клика по веб-документу
		let div = $(".header-bottom__mobile-menu"); // тут указываем ID элемента
		if (!div.is(e.target) && div.has(e.target).length === 0) { // если клик был не по нашему блоку
			div.slideUp('open');
			$('.header-bottom__mobile').removeClass('open');
		}
	});

	$('.filter__name').click(function () {
		$(this).closest('.filter').toggleClass('open');
	});



	$(document).mouseup(function (e) { // событие клика по веб-документу
		var div = $(".filter__select"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
			&& div.has(e.target).length === 0) { // и не по его дочерним элементам
			div.closest('.filter').removeClass('open');
		}
	});

	$(document).mouseup(function (e) { // событие клика по веб-документу
		var div = $(".header-phones__back"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
			&& div.has(e.target).length === 0) { // и не по его дочерним элементам
			div.removeClass('open');
			$('.header-top__phones > .icon-cross.open').removeClass('open');
			$('html, body').removeClass('overflow-hidden');
		}
	});



	$('body').on('click', '.header-phones__front,  .header-phones__back .close-mob', function () {
		$(this).addClass('open');
		$('.header-phones__back').addClass('open');
		$('html, body').addClass('overflow-hidden');
	});

	$('body').on('click', '.header-phones__front.open', function () {
		$(this).removeClass('open');
		$('.header-phones__back').removeClass('open');
		$('html, body').removeClass('overflow-hidden');
	});


	if ($('.styler')) {
		$('.styler').styler();
	}

	$('[data-tab-on="on"] [data-li]').click(function () {

		let dataLi = $(this).attr('data-li');

		if (window.location.pathname == '/services/transaction_support/mediatsiya/' || window.location.pathname == '/exchange_info/count_analitics/informatsionnye-uslugi/' || window.location.pathname == '/by/services/transaction_support/mediatsiya/' || window.location.pathname == '/by/exchange_info/count_analitics/informatsionnye-uslugi/') {

			if (!$(this).hasClass('active')) {
				$(this).addClass('active').siblings().removeClass('active');

				$(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').addClass('active').siblings().removeClass('active');

				$('.timetables-market').nextAll().hide();

			}

			else if ($(this).hasClass('active')) {
				$(this).removeClass('active');
				$(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').removeClass('active');
				$('.timetables-market').nextAll().show();
			}
		}

		else {
			if (!$(this).hasClass('active')) {
				$(this).addClass('active').siblings().removeClass('active');

				$(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').addClass('active').siblings().removeClass('active');
			}
		}



	});

	$('.timetables-market__filter').slick({
		slidesToShow: 3,
		centerMode: true,
		infinite: false,
		initialSlide: 1,
	});

	// appender
	function appender() {
		let width = $(window).width();
		let height = $(window).height();

		if (width > 1220) {
			$('.header').attr('data-mob', 'false');
			menuDex();
		}
		else {
			$('.header').attr('data-mob', 'true');
			menuMob();
		}
	}
	appender();

	$(window).resize(appender);

	function menuMob() {
		$('.header-bottom__container').prepend($('.header-top__phones'));
		$('.header-bottom__mobile-menu .container').append($('.header-top__btns'));
		$('.header-bottom__mobile-menu .container').append($('.header-bottom__menu'));
	}

	function menuDex() {
		$('.header-top__container').prepend($('.header-top__phones'));
		$('.header-top__container').append($('.header-top__btns'));
		$('.header-bottom__container').append($('.header-bottom__menu'));


	}

	// menu mobile handler
	$('body').on('click', '.header-bottom__mobile .icon-mobile-menu', function () {
		$('.header-bottom__mobile').toggleClass('open');
		$('.header-bottom__mobile-menu').slideToggle();
		$('html, body').addClass('overflow-hidden');
	});

	$('body').on('click', '.header-bottom__mobile .icon-cross', function () {
		$('html, body').removeClass('overflow-hidden');
	});

	if ($('.flipbook').length) {
		$(".flipbook").turn({
			width: 800,
			height: 800,
			autoCenter: true
		});
	}

	$('body').on('click', '.header-btns__item--calendar > .icon, .header-btns__item--calendar > span ', function () {
		$('.popup-calendar').toggleClass('show');
		return false;
	});

	$(document).mouseup(function (e) { // событие клика по веб-документу
		var div = $(".header-btns__item--calendar"); // тут указываем ID элемента
		if (!div.is(e.target) // если клик был не по нашему блоку
			&& div.has(e.target).length === 0) { // и не по его дочерним элементам
			$('.popup-calendar').removeClass('show');
		}
	});

	if (width <= 1480) {
		$('body').on('click', '.header-btns__item--lang', function () {
			if (!$(this).hasClass('open')) {
				$(this).addClass('open');
				return false;
			} else {
				$(this).removeClass('open');
			}
		})
	}


	// КАЛЕНДАРЬ (на бою вынесен в хедер)
	var eventData = [
		{ "date": "2019-12-26", 'active': 'false' },
		{ "date": "2019-12-25" }
	];
	//
	// $(".butb--calendar").zabuto_calendar({
	// 	cell_border: false,
	// 	data: eventData,
	// 	show_days: false,
	// 	language: "RU",
	// 	weekstartson: 1,
	// 	today: true,
	// 	action: function () {
	//
	// 		alert(this.id.replace(/date_/g, ""))
	//
	// 	},
	// 	action_nav: function () {
	// 	},
	// 	nav_icon: {
	// 		prev: '<i class="fa fa-chevron-left"></i>',
	// 		next: '<i class="fa fa-chevron-right"></i>'
	// 	}
	// });

	mobContHeight();

	$('.ajax-popup-link').magnificPopup({
		type: 'ajax',
		alignTop: false,
		overflowY: 'hidden',
		fixedContentPos: true,
		fixedBgPos: true,
		callbacks: {
			ajaxContentAdded: function () {
				if ($(window).width() > 1180) {
					if ($("#flipbook").length) {
						$("#flipbook").turn({
							width: 1100,
							height: 777,
							next: true
						});
						$("#prev").click(function (e) {
							e.preventDefault();
							$("#flipbook").turn("previous");
						});

						$("#next").click(function (e) {
							e.preventDefault();
							$("#flipbook").turn("next");
						});
					};
				}
				else {
					if ($("#flipbook").length) {
						$("#flipbook").turn({
							width: 320,
							height: 226,
							next: true
						});
						$("#prev").click(function (e) {
							e.preventDefault();
							$("#flipbook").turn("previous");
						});

						$("#next").click(function (e) {
							e.preventDefault();
							$("#flipbook").turn("next");
						});
					};
				}

			},
		}
	});

	// function footerAccordion() {
	// 	if($(window).width() <= 450) {
	// 		$('.footer-item').addClass('accordion');
	// 		$('.footer-item-zag').addClass('accordion-head');
	// 		$('.footer-item > ul').addClass(('accordion-body'));
	// 	}
	//
	// 	else if($(Window).width() > 450) {
	// 		$('.footer-item').removeClass('accordion');
	// 		$('.footer-item-zag').removeClass('accordion-head');
	// 		$('.footer-item > ul').removeClass(('accordion-body'));
	// 	}
	// }
	// footerAccordion();
	// $(window).resize(footerAccordion);

	$('body').on('click', '.header-tabs__menu > li', function () {
		$(this).find('.dropmenu').slideToggle();
		$(this).find('.icon').toggleClass('open');
	});

	$('.img-expand').magnificPopup({ type: 'image' });

	$('.popup-youtube').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade video-style',
		removalDelay: 160,
		preloader: false,
		fixedContentPos: true
	});


	$('body').on('click', '.text-page__menu .has-drop > a', function () {
		$(this).closest('.has-drop').toggleClass('open');
		$(this).siblings('ul').slideToggle(300);

		return false;
	});



	$('body').on('click', '.header-bottom__menu ul li .ar', function () {
		$(this).closest('li').toggleClass('open');
		$(this).addClass('open').siblings('.dropdown').slideToggle(500);

	});

	badsee();

	trate();


	findMaxHeight();

	// $('.broker-table__col .show-detail').mouseenter(function(){
	// 	$(this).closest('.broker-table__col').find('.brocker-detail').addClass('show')
	// })

	$(document).on('click', '.brocker-detail .icon', function(){
		$.magnificPopup.close();
	})

});


$(window).resize(function () {
	let width = $(window).width();
	let height = $(window).height();

	mainSliderNavMargin(width);
	mobContHeight();
	findMaxHeight();

});

$(window).scroll(function () {
	let width = $(window).width();
	let height = $(window).height();

});



function mainSliderNavMargin(width) {

	let offsetContainer = (width - $('.container').width()) / 2;
	let padding = parseInt($('.main-slider__nav .slick-slide').css('padding-right'));

	$('.main-slider__nav').css('margin-right', offsetContainer - padding + 'px');

}

// Функция расчета высоты блока контактов
function mobContHeight() {
	if ($(window).width() <= 1220) {

		let hH = $('.header').outerHeight();
		let blockH = $(window).height() - hH;



		$('.header-phones__back').css('height', blockH + 'px');
		$('.header-bottom__mobile-menu').css('height', blockH + 'px');

		// Для блока контактов
		// let contHeight = $('.header-phones__back .container').outerHeight();

		// let calcHeight = contHeight - headHeight - 20;

		//
		// // Для блока меню
		// let calcMenu = $(window).height() - headHeight - 12;

	}
}
$(document).ready(function () {
	tpMenu();

	$('.simple-ajax-popup-align-top').magnificPopup({
		type: 'ajax',
	});

	$('.ajax-popup-link-broker').magnificPopup({
		type: 'ajax',
		showCloseBtn: false
	});
});


/* Боковое меню в адаптиве */
function tpMenu() {
	$('body').on('click', '.js-tpmenu', function () {
		$(this).toggleClass('text-page__menu-btn--active');
		$('.text-page__menu').toggleClass('text-page__menu--active');
		// $('html, body').toggleClass('overflow-hidden');

		return false;
	});
}

function badsee() {
	var $body = $('body');
	var $badseeConfig = $('.badsee-config');


	// включение режима и переключатель настроек
	$body.on('click', '.header-btns__item--eye', function () {
		if (!$body.hasClass('badsee')) {
			$body.addClass('badsee');
			$body.addClass('badsee-cs-light');
			$badseeConfig.addClass('badsee-show');
			$('.badsee-ff').addClass('active');
			$('.badsee-fs').addClass('active');
			$('.badsee-ls').addClass('active');
			$('.badsee-light').addClass('active');
			document.cookie = 'badsee=active; path=/;';
			document.cookie = 'badsee-ff=default; path=/;';
			document.cookie = 'badsee-fs=default; path=/;';
			document.cookie = 'badsee-ls=default; path=/;';
			document.cookie = 'badsee-cs=light; path=/;';

		}

		else if ($body.hasClass('badsee')) {
			$badseeConfig.toggleClass('badsee-show');
		}

		return false;
	});

	// выключение режима
	$('.badsee-config').on('click', '.badsee-disable', function () {

		document.cookie = 'badsee=active; path=/; max-age=0';
		document.cookie = 'badsee-ff=default; path=/; max-age=0';
		document.cookie = 'badsee-fs=default; path=/; max-age=0';
		document.cookie = 'badsee-ls=default; path=/; max-age=0';
		document.cookie = 'badsee-cs=light; path=/; max-age=0';

		$body.removeClass('badsee badsee-ff-arial badsee-ff-times badsee-fs-md badsee-fs-lg badsee-ls-md badsee-ls-lg badsee-cs-light badsee-cs-dark badsee-cs-blue');

		$('.badsee-config button').removeClass('active');
		$('.badsee-config').removeClass('badsee-show');

	});

	// закрытие настроек
	$body.on('click', '.badsee-close', function () {
		$('.badsee-config').removeClass('badsee-show');
	});

	// переключатель кнопок
	$body.on('click', '.badsee-w25 button', function () {
		$(this).closest('.badsee-w25').find('button').removeClass('active');
		$(this).addClass('active');
	});

	// изменение шрифта
	$body.on('click', '.badsee-ff', function () {
		document.cookie = 'badsee-ff=default; path=/;';
		$body.removeClass('badsee-ff-arial');
		$body.removeClass('badsee-ff-times');
	});

	$body.on('click', '.badsee-ff-arial', function () {
		document.cookie = 'badsee-ff=arial; path=/;';
		$body.removeClass('badsee-ff-times');
		$body.addClass('badsee-ff-arial');
	});

	$body.on('click', '.badsee-ff-times', function () {
		document.cookie = 'badsee-ff=times; path=/;';
		$body.removeClass('badsee-ff-arial');
		$body.addClass('badsee-ff-times');
	});

	// изменение размера
	$body.on('click', '.badsee-fs', function () {
		document.cookie = 'badsee-fs=default; path=/;';
		$body.removeClass('badsee-fs-md');
		$body.removeClass('badsee-fs-lg');
	});

	$body.on('click', '.badsee-fs-md', function () {
		document.cookie = 'badsee-fs=md; path=/;';
		$body.addClass('badsee-fs-md');
		$body.removeClass('badsee-fs-lg');
	});

	$body.on('click', '.badsee-fs-lg', function () {
		document.cookie = 'badsee-fs=lg; path=/;';
		$body.removeClass('badsee-fs-md');
		$body.addClass('badsee-fs-lg');
	});

	// изменение интервала
	$body.on('click', '.badsee-ls', function () {
		document.cookie = 'badsee-ls=default; path=/;';
		$body.removeClass('badsee-ls-md');
		$body.removeClass('badsee-ls-lg');
	});

	$body.on('click', '.badsee-ls-md', function () {
		document.cookie = 'badsee-ls=md; path=/;';
		$body.addClass('badsee-ls-md');
		$body.removeClass('badsee-ls-lg');
	});

	$body.on('click', '.badsee-ls-lg', function () {
		document.cookie = 'badsee-ls=lg; path=/;';
		$body.removeClass('badsee-ls-md');
		$body.addClass('badsee-ls-lg');
	});

	// изменение цветовой схемы
	$body.on('click', '.badsee-light', function () {
		document.cookie = 'badsee-cs=light; path=/;';
		$body.removeClass('badsee-cs-dark');
		$body.removeClass('badsee-cs-blue');
		$body.addClass('badsee-cs-light');
	});

	$body.on('click', '.badsee-dark', function () {
		document.cookie = 'badsee-cs=dark; path=/;';
		$body.addClass('badsee-cs-dark');
		$body.removeClass('badsee-cs-blue');
		$body.removeClass('badsee-cs-light');
	});

	$body.on('click', '.badsee-blue', function () {
		document.cookie = 'badsee-cs=blue; path=/;';
		$body.addClass('badsee-cs-blue');
		$body.removeClass('badsee-cs-dark');
		$body.removeClass('badsee-cs-light');
	});
}

function trate() {
	const sortBtn = '.trate-sort';

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+2)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(255, 76, 76, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+3)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(76, 195, 255, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+4)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(136, 255, 76, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+5)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(255, 225, 76, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+6)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(179, 89, 149, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('.trate .trate-r:nth-child(n+3) .trate-c:nth-child(6n+7)').each(function () {
		if ($(this).text() <= 10)
			$(this).css('background-color', 'rgba(134, 89, 179, ' + (10 - $(this).text() + 1) / 10 + ')');
	});

	$('body').on('click', sortBtn, function () {
		let colNum = $(this).closest('.trate-c').index() + 1;

		$('.trate-r:nth-child(n+3)').each(function () {
			$(this).find('.trate-c:nth-child(' + colNum + ')');
			let curNum = $(this).find('.trate-c:nth-child(' + colNum + ')').text();
			curNum = Number(curNum);

			if (Number.isInteger(curNum)) {
				$(this).css('order', curNum);
			}
			else {
				$(this).css('order', '999');
			}

		});

		return false;
	});
}

function findMaxHeight() {
	let height = 0;

	if ($(window).width() > 600) {
		$('.timetables-market__item.v--5 li a').each(function () {
			if ($(this).height() > height) height = $(this).height();
		});

		$('.timetables-market__item.v--5 li a').height(height);
	}
	else $('.timetables-market__item.v--5 li a').height('auto');
}

$('body').on("click", '.ajax-popup', function(e){
	e.preventDefault();
	var file_link = $(this).attr('href');

	let FormClass = ['ajaxForm'];

	if($(this).attr('data-form') == 'regisration'){

		FormClass.push('regForm');
		$.magnificPopup.open({
			items: {
				src: file_link,
			},
			type: 'ajax',
			removalDelay: 300,
			mainClass: FormClass,
			callbacks: {
				ajaxContentAdded: function() {
					styler();
				}
			}
		});
	}
	else{
		$.magnificPopup.open({
			items: {
				src: file_link,
			},
			type: 'ajax',
			removalDelay: 300,
			mainClass: FormClass,
			callbacks: {
				ajaxContentAdded: function() {
					styler();
				}
			}
		});
	}
});
function styler() {
	$('.styler').styler();

	$('.styler[type="file"]').on('change', function () {

		if($(this).val() != ''){
			$(this).closest('.lc-data__item-block').addClass('no-empty');
			$(this).closest('.jq-file').find('.jq-file__browse').text('Загрузить другой');
		}

	});

}
$('body').on('click', '.reset-pw__link', function() {
	$('.reset-pw__form').slideToggle();
});
