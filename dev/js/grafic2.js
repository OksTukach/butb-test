const list = {
    prod1: {
        week1: [[Date.parse("2020-07-20"), 29.9], [Date.parse("2020-07-21"), 71.5], [Date.parse("2020-07-22"), 106.4], [Date.parse("2020-07-23"), 129.2], [Date.parse("2020-07-24"), 144.0], [Date.parse("2020-07-25"), 176.0], [Date.parse("2020-07-26"), 176.056]],
        month: [[Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.053], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.056], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.223], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 0.298], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 0.056], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 0.474], [Date.parse("2020-07-30"), 0.2756]],
        month3: [[Date.parse("2020-05-1"), 0.9432], [Date.parse("2020-05-2"), 0.587], [Date.parse("2020-05-3"), 0.4746], [Date.parse("2020-05-4"), 0.256], [Date.parse("2020-05-5"), 0.053], [Date.parse("2020-05-6"), 0.075], [Date.parse("2020-05-7"), 0.056], [Date.parse("2020-05-8"), 0.97567], [Date.parse("2020-05-9"), 0.575], [Date.parse("2020-05-10"), 0.423], [Date.parse("2020-05-11"), 0.223], [Date.parse("2020-05-12"), 0.032], [Date.parse("2020-05-13"), 1.0897], [Date.parse("2020-05-14"), 0.073], [Date.parse("2020-05-15"), 0.0543], [Date.parse("2020-05-16"), 0.056], [Date.parse("2020-05-17"), 0.96534], [Date.parse("2020-05-18"), 2.5453], [Date.parse("2020-05-19"), 0.4634], [Date.parse("2020-05-20"), 0.207], [Date.parse("2020-05-21"), 0.298], [Date.parse("2020-05-22"), 0.0123], [Date.parse("2020-05-23"), 0.035], [Date.parse("2020-05-24"), 0.00987], [Date.parse("2020-05-25"), 0.056], [Date.parse("2020-05-26"), 0.056423], [Date.parse("2020-05-27"), 0.942], [Date.parse("2020-05-28"), 0.5423], [Date.parse("2020-05-29"), 0.474], [Date.parse("2020-05-30"), 0.2756], [Date.parse("2020-06-1"), 0.9432], [Date.parse("2020-06-2"), 0.587], [Date.parse("2020-06-3"), 0.4746], [Date.parse("2020-06-4"), 0.256], [Date.parse("2020-06-5"), 0.053], [Date.parse("2020-06-6"), 0.075], [Date.parse("2020-06-7"), 0.056], [Date.parse("2020-06-8"), 0.97567], [Date.parse("2020-06-9"), 0.575], [Date.parse("2020-06-10"), 0.423], [Date.parse("2020-06-11"), 0.223], [Date.parse("2020-06-12"), 0.032], [Date.parse("2020-06-13"), 1.0897], [Date.parse("2020-06-14"), 0.073], [Date.parse("2020-06-15"), 0.0543], [Date.parse("2020-06-16"), 0.056], [Date.parse("2020-06-17"), 0.96534], [Date.parse("2020-06-18"), 2.5453], [Date.parse("2020-06-19"), 0.4634], [Date.parse("2020-06-20"), 0.207], [Date.parse("2020-06-21"), 0.298], [Date.parse("2020-06-22"), 0.0123], [Date.parse("2020-06-23"), 0.035], [Date.parse("2020-06-24"), 0.00987], [Date.parse("2020-06-25"), 0.056], [Date.parse("2020-06-26"), 0.056423], [Date.parse("2020-06-27"), 0.942], [Date.parse("2020-06-28"), 0.5423], [Date.parse("2020-06-29"), 0.474], [Date.parse("2020-06-30"), 0.2756], [Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.053], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.056], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.223], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 0.298], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 0.056], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 0.474], [Date.parse("2020-07-30"), 0.2756]]
    },
    prod2: {
        week1: [[Date.parse("2020-07-20"), 5.9], [Date.parse("2020-07-21"), 4.5456], [Date.parse("2020-07-22"), 5.4456], [Date.parse("2020-07-23"), 4.2456], [Date.parse("2020-07-24"), 4.0], [Date.parse("2020-07-25"), 5.0], [Date.parse("2020-07-26"), 5.056]],
        month: [[Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.053], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.056], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.223], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 0.999], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 1.9999], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 0.474], [Date.parse("2020-07-30"), 8.2756]],
        month3: [[Date.parse("2020-05-1"), 0.9432], [Date.parse("2020-05-2"), 0.587], [Date.parse("2020-05-3"), 0.4746], [Date.parse("2020-05-4"), 0.256], [Date.parse("2020-05-5"), 0.053], [Date.parse("2020-05-6"), 0.075], [Date.parse("2020-05-7"), 0.056], [Date.parse("2020-05-8"), 0.97567], [Date.parse("2020-05-9"), 0.575], [Date.parse("2020-05-10"), 0.423], [Date.parse("2020-05-11"), 0.223], [Date.parse("2020-05-12"), 0.032], [Date.parse("2020-05-13"), 1.6456], [Date.parse("2020-05-14"), 0.073], [Date.parse("2020-05-15"), 0.0543], [Date.parse("2020-05-16"), 0.056], [Date.parse("2020-05-17"), 0.96534], [Date.parse("2020-05-18"), 2.5453], [Date.parse("2020-05-19"), 0.4634], [Date.parse("2020-05-20"), 0.207], [Date.parse("2020-05-21"), 0.298], [Date.parse("2020-05-22"), 0.0123], [Date.parse("2020-05-23"), 0.035], [Date.parse("2020-05-24"), 0.00987], [Date.parse("2020-05-25"), 0.056], [Date.parse("2020-05-26"), 0.056423], [Date.parse("2020-05-27"), 0.942], [Date.parse("2020-05-28"), 0.5423], [Date.parse("2020-05-29"), 0.4765464], [Date.parse("2020-05-30"), 0.2756], [Date.parse("2020-06-1"), 0.9432], [Date.parse("2020-06-2"), 0.587], [Date.parse("2020-06-3"), 0.9999], [Date.parse("2020-06-4"), 0.256], [Date.parse("2020-06-5"), 0.053], [Date.parse("2020-06-6"), 0.075], [Date.parse("2020-06-7"), 0.056], [Date.parse("2020-06-8"), 0.97567], [Date.parse("2020-06-9"), 0.575], [Date.parse("2020-06-10"), 0.423], [Date.parse("2020-06-11"), 0.223], [Date.parse("2020-06-12"), 0.032], [Date.parse("2020-06-13"), 1.0897], [Date.parse("2020-06-14"), 0.073], [Date.parse("2020-06-15"), 0.0543], [Date.parse("2020-06-16"), 0.056], [Date.parse("2020-06-17"), 0.96534], [Date.parse("2020-06-18"), 2.5453], [Date.parse("2020-06-19"), 0.4634], [Date.parse("2020-06-20"), 0.207], [Date.parse("2020-06-21"), 0.298], [Date.parse("2020-06-22"), 0.0123], [Date.parse("2020-06-23"), 0.035], [Date.parse("2020-06-24"), 0.00987], [Date.parse("2020-06-25"), 0.056], [Date.parse("2020-06-26"), 0.056423], [Date.parse("2020-06-27"), 0.942], [Date.parse("2020-06-28"), 0.5423], [Date.parse("2020-06-29"), 0.474], [Date.parse("2020-06-30"), 0.2756], [Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.423423], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.534534], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.54354], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 0.298], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 0.423432], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 0.474], [Date.parse("2020-07-30"), 0.2756]]
    },
    prod3: {
        week1: [[Date.parse("2020-07-20"), 99.9465], [Date.parse("2020-07-21"), 99.5465], [Date.parse("2020-07-22"), 99.4465], [Date.parse("2020-07-23"), 99.2465], [Date.parse("2020-07-24"), 99.0465], [Date.parse("2020-07-25"), 99.0465], [Date.parse("2020-07-26"), 99.046556]],
        month: [[Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.053], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.056], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.223], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 53.534534], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 0.056], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 7345.474], [Date.parse("2020-07-30"), 0.2756]],
        month3: [[Date.parse("2020-05-1"), 0.9432], [Date.parse("2020-05-2"), 0.587], [Date.parse("2020-05-3"), 0.4746], [Date.parse("2020-05-4"), 0.256], [Date.parse("2020-05-5"), 0.053], [Date.parse("2020-05-6"), 0.075], [Date.parse("2020-05-7"), 0.056], [Date.parse("2020-05-8"), 0.97567], [Date.parse("2020-05-9"), 0.575], [Date.parse("2020-05-10"), 0.423], [Date.parse("2020-05-11"), 0.223], [Date.parse("2020-05-12"), 0.032], [Date.parse("2020-05-13"), 1.5345], [Date.parse("2020-05-14"), 0.073], [Date.parse("2020-05-15"), 0.0543], [Date.parse("2020-05-16"), 0.056], [Date.parse("2020-05-17"), 0.96534], [Date.parse("2020-05-18"), 2.5453], [Date.parse("2020-05-19"), 0.4634], [Date.parse("2020-05-20"), 0.207], [Date.parse("2020-05-21"), 0.298], [Date.parse("2020-05-22"), 0.0123], [Date.parse("2020-05-23"), 0.035], [Date.parse("2020-05-24"), 0.00987], [Date.parse("2020-05-25"), 0.534534], [Date.parse("2020-05-26"), 0.056423], [Date.parse("2020-05-27"), 0.942], [Date.parse("2020-05-28"), 0.5423], [Date.parse("2020-05-29"), 0.474], [Date.parse("2020-05-30"), 0.2756], [Date.parse("2020-06-1"), 0.9432], [Date.parse("2020-06-2"), 0.587], [Date.parse("2020-06-3"), 0.534], [Date.parse("2020-06-4"), 0.256], [Date.parse("2020-06-5"), 0.053], [Date.parse("2020-06-6"), 0.075], [Date.parse("2020-06-7"), 0.056], [Date.parse("2020-06-8"), 0.97567], [Date.parse("2020-06-9"), 0.575], [Date.parse("2020-06-10"), 0.423], [Date.parse("2020-06-11"), 0.223], [Date.parse("2020-06-12"), 0.032], [Date.parse("2020-06-13"), 1.0897], [Date.parse("2020-06-14"), 0.073], [Date.parse("2020-06-15"), 53.0543], [Date.parse("2020-06-16"), 0.056], [Date.parse("2020-06-17"), 0.96534], [Date.parse("2020-06-18"), 2.5453], [Date.parse("2020-06-19"), 0.4634], [Date.parse("2020-06-20"), 0.207], [Date.parse("2020-06-21"), 0.298], [Date.parse("2020-06-22"), 0.0123], [Date.parse("2020-06-23"), 0.035], [Date.parse("2020-06-24"), 0.00987], [Date.parse("2020-06-25"), 0.056], [Date.parse("2020-06-26"), 0.056423], [Date.parse("2020-06-27"), 0.942], [Date.parse("2020-06-28"), 0.5423], [Date.parse("2020-06-29"), 0.474], [Date.parse("2020-06-30"), 0.2756], [Date.parse("2020-07-1"), 0.9432], [Date.parse("2020-07-2"), 0.587], [Date.parse("2020-07-3"), 0.4746], [Date.parse("2020-07-4"), 0.256], [Date.parse("2020-07-5"), 0.053], [Date.parse("2020-07-6"), 0.075], [Date.parse("2020-07-7"), 0.056], [Date.parse("2020-07-8"), 0.97567], [Date.parse("2020-07-9"), 0.575], [Date.parse("2020-07-10"), 0.423], [Date.parse("2020-07-11"), 0.223], [Date.parse("2020-07-12"), 0.032], [Date.parse("2020-07-13"), 1.0897], [Date.parse("2020-07-14"), 0.073], [Date.parse("2020-07-15"), 0.0543], [Date.parse("2020-07-16"), 0.056], [Date.parse("2020-07-17"), 0.96534], [Date.parse("2020-07-18"), 2.5453], [Date.parse("2020-07-19"), 0.4634], [Date.parse("2020-07-20"), 0.207], [Date.parse("2020-07-21"), 0.298], [Date.parse("2020-07-22"), 0.0123], [Date.parse("2020-07-23"), 0.035], [Date.parse("2020-07-24"), 0.00987], [Date.parse("2020-07-25"), 0.056], [Date.parse("2020-07-26"), 0.056423], [Date.parse("2020-07-27"), 0.942], [Date.parse("2020-07-28"), 0.5423], [Date.parse("2020-07-29"), 0.474], [Date.parse("2020-07-30"), 0.2756]]
    }
};

const defaultChart = [[Date.parse("2020-07-20"), 29.9], [Date.parse("2020-07-21"), 71.5], [Date.parse("2020-07-22"), 106.4], [Date.parse("2020-07-23"), 129.2], [Date.parse("2020-07-24"), 144.0], [Date.parse("2020-07-25"), 176.0], [Date.parse("2020-07-26"), 176.056]];


var chart, chart2, chart3;

$(document).ready(function () {

    var prod = $('[data-prod].active').attr('data-prod');
    var temp = $('.grafic-js__temp input:checked').val();
    var touchtime = 0;
    Highcharts.setOptions({
        lang: {
            loading: 'Загрузка...',
            months: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            weekdays: ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'],
            shortMonths: ['Янв', 'Фев', 'Март', 'Апр', 'Май', 'Июнь', 'Июль', 'Авг', 'Сент', 'Окт', 'Нояб', 'Дек'],
            exportButtonTitle: "Экспорт",
            printButtonTitle: "Печать",
            rangeSelectorFrom: "С",
            rangeSelectorTo: "По",
            rangeSelectorZoom: "Период",
            downloadPNG: 'Скачать PNG',
            downloadJPEG: 'Скачать JPEG',
            downloadPDF: 'Скачать PDF',
            downloadSVG: 'Скачать SVG',
            printChart: 'Напечатать график'
        }
    });

    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'myChart'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            type: "datetime",
            title: {
                text: null
            },
            labels: {
                formatter: function formatter() {
                    return Highcharts.dateFormat('%b %e', this.value);
                }
            }
        },
        yAxis: {
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                color: 'green'
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 1,
                        y1: 1,
                        x2: 1,
                        y2: 1
                    },
                    stops: [[0, '#4DAA59'], [1, '#4DAA59']]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        series: [{
            type: 'area',
            name: 'Пунктов',
            data: defaultChart
        }]
    });
    chart2 = new Highcharts.Chart({
        chart: {
            renderTo: 'myChart2'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            type: "datetime",
            title: {
                text: null
            },
            labels: {
                formatter: function formatter() {
                    return Highcharts.dateFormat('%b %e', this.value);
                }
            }
        },
        yAxis: {
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                color: 'green'
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops: [[0, '#4DAA59'], [1, '#fff']]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        series: [{
            type: 'area',
            name: 'Пунктов',
            data: defaultChart
        }]
    });
    chart3 = new Highcharts.Chart({
        chart: {
            renderTo: 'myChart3'
        },
        title: {
            text: null
        },
        subtitle: {
            text: null
        },
        xAxis: {
            type: "datetime",
            title: {
                text: null
            },
            labels: {
                formatter: function formatter() {
                    return Highcharts.dateFormat('%b %e', this.value);
                }
            }
        },
        yAxis: {
            title: {
                text: null
            }
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                color: 'green'
            },
            area: {
                fillColor: {
                    linearGradient: {
                        x1: 0,
                        y1: 0,
                        x2: 1,
                        y2: 1
                    },
                    stops: [[0, 'transparent'], [1, 'transparent']]
                },
                marker: {
                    radius: 2
                },
                lineWidth: 1,
                states: {
                    hover: {
                        lineWidth: 1
                    }
                },
                threshold: null
            }
        },
        series: [{
            type: 'area',
            name: 'Пунктов',
            data: defaultChart
        }]
    });


    $('body').on('click', '.grafic-list [data-href]:not(.active)', function () {
        const parent = $(this).closest('.grafic');
        const elemID = parent.find('[data-highcharts-chart]').attr('id');

        parent.find('.grafic-list [data-href].active').removeClass('active');
        $(this).addClass('active');


        const temp = parent.find('.grafic-js__temp input[type="radio"]:checked').val();
        const prod = parent.find('[data-prod].active').attr('data-prod');
        const actual = list[prod][temp];

        console.log(prod)
        console.log(temp)


        updateGrafic(elemID, actual);

    });
    $('body').on('change', '.grafic-js__temp input[type="radio"]', function () {
        var parent = $(this).closest('.grafic');
        const temp = $(this).val();
        const prod = parent.find('[data-prod].active').attr('data-prod');
        const actual = list[prod][temp];

        var elemID = parent.find('[data-highcharts-chart]').attr('id');

        updateGrafic(elemID, actual);
    });

    if ($(window).width() > 990) {
        $('body').on('dblclick ', '.grafic-list [data-href]', function () {
            var link = $(this).attr('data-href');
            window.open(link, '_blank');
        });
    } else {


        $('body').on('click', '.grafic-list [data-href]', function () {
            if (touchtime == 0) {
                touchtime = new Date().getTime();
            } else {
                if (((new Date().getTime()) - touchtime) < 800) {
                    var link = $(this).attr('data-href');
                    window.open(link, '_blank');
                    touchtime = 0;
                } else {
                    touchtime = 0;
                }
            }
        });

        // $('body').on('taphold', '.grafic-list [data-href]', function () {
        //
        //     console.log('fdsf')
        //
        // });
        var pressTimer;
        $(".grafic-list [data-href]").on('touchend', function (e) {
            clearTimeout(pressTimer);
        }).on('touchstart', function (e) {

            var tooltip = $(this).find('.grafic-list__tooltip');

            var target = $(e.currentTarget);
            pressTimer = window.setTimeout(function () {
                $('.grafic-list__tooltip.show').removeClass('show');
                tooltip.addClass('show');

                return false;
            }, 500)
        });

        $('body').on('click', '.grafic-list__tooltip', function () {
            $(this).removeClass('show');
            return false;
        });

    }


    $('body').on('click', '.grafic-js__opener', function (){

        var graficJs = $(this).closest('.grafic').find('.grafic-js');
        if($(this).hasClass('open')){
            $(this).removeClass('open').find('span').text('Открыть график');
            graficJs.slideUp(300)
        }
        else{
            $(this).addClass('open').find('span').text('Закрыть график');
            graficJs.slideDown(300)
        }


    });


});


function updateGrafic(id, actual) {

    if (id == 'myChart') {
        chart.update({
            series: [{
                data: actual
            }]
        });
    }
    if (id == 'myChart2') {
        chart2.update({
            series: [{
                data: actual
            }]
        });
    }
    if (id == 'myChart3') {
        chart3.update({
            series: [{
                data: actual
            }]
        });
    }


}