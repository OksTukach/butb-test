module.exports = function () {
	$.gulp.task('watch', function () {
		$.gulp.watch('./dev/pug/**/*.pug', $.gulp.series('pug'));
		$.gulp.watch('./dev/sass/**/*.scss', $.gulp.series('styles-sass:dev'));
		$.gulp.watch('./dev/img/svg/*.svg', $.gulp.series('svg'));
		$.gulp.watch('./dev/libs/**/*.{css,js}', $.gulp.series('libs-сss:dev', 'libs-all-сss:dev','libs-js:dev', 'libs-all-js:dev', ));
		$.gulp.watch('./dev/js/**/*.js', $.gulp.series('js:copy:dev'));
		$.gulp.watch(['./dev/img/general/**/*.{png,jpg,gif,svg}', './dev/img/content/**/*.{png,jpg,gif,svg}', './dev/img/**/*.{png,jpg,gif,svg}'],
			$.gulp.series('img:dev'));
	});
};