"use strict";

$(document).ready(function () {
  var width = $(window).width();
  var height = $(window).height();
  console.log('Width: ' + width + 'px');
  console.log('Height: ' + height + 'px');
  svg4everybody();
  $('body').on('click', '.header-top__tabs li:not(active)', function () {
    if ($(window).width() > 1220) {
      var dataTab = $(this).attr('data-tab');
      $(this).addClass('active');
      $(this).siblings().removeClass('active');
      $('.header-tabs-container[data-tab="' + dataTab + '"]').addClass('active');
      $('.header-tabs-container[data-tab="' + dataTab + '"]').siblings().removeClass('active');
    }
  });
  $('body').on('click', '.header-top__tabs li', function () {
    if ($(window).width() <= 1220) {
      $(this).toggleClass('open');
      $(this).find('.header-tabs-container').slideToggle();
    }
  });
  $('.partners-slider').slick({
    slidesToShow: 3,
    prevArrow: '<svg class="slick-prev" width="23" height="13" viewBox="0 0 23 13" xmlns="http://www.w3.org/2000/svg"><path d="M6.72003 11.9911C6.94485 12.2222 6.94485 12.5957 6.72003 12.8267C6.4952 13.0578 6.1318 13.0578 5.90698 12.8267L0.16905 6.9299C0.0603751 6.81998 -7.54858e-08 6.66989 -7.73602e-08 6.51271C-7.74095e-08 6.50857 0.00230209 6.50443 0.00230209 6.5003C0.00230209 6.49616 -7.75998e-08 6.49261 -7.76562e-08 6.48789C-8.00027e-08 6.29111 0.0994758 6.12565 0.243225 6.0181L5.93055 0.173289C6.15537 -0.0577619 6.51878 -0.0577619 6.7436 0.173289C6.96843 0.404339 6.96843 0.777803 6.7436 1.00885L1.9872 5.89696L22.425 5.89696C22.743 5.89696 23 6.16111 23 6.48789C23 6.81467 22.743 7.07881 22.425 7.07881L1.9389 7.07881L6.72003 11.9911Z" /></svg>',
    nextArrow: '<svg class="slick-next" width="23" height="13" viewBox="0 0 23 13" xmlns="http://www.w3.org/2000/svg"><path d="M16.28 11.9911C16.0551 12.2222 16.0551 12.5957 16.28 12.8267C16.5048 13.0578 16.8682 13.0578 17.093 12.8267L22.8309 6.9299C22.9396 6.81998 23 6.66989 23 6.51271C23 6.50857 22.9977 6.50443 22.9977 6.5003C22.9977 6.49616 23 6.49261 23 6.48789C23 6.29111 22.9005 6.12565 22.7568 6.0181L17.0695 0.173289C16.8446 -0.0577619 16.4812 -0.0577619 16.2564 0.173289C16.0316 0.404339 16.0316 0.777803 16.2564 1.00885L21.0128 5.89696L0.575 5.89696C0.257025 5.89696 8.1553e-08 6.16111 7.76562e-08 6.48789C7.37594e-08 6.81467 0.257025 7.07881 0.575 7.07881L21.0611 7.07881L16.28 11.9911Z" /></svg>',
    responsive: [{
      breakpoint: 768,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 500,
      settings: {
        slidesToShow: 1
      }
    }]
  });
  $('.main-slider__grand').slick({
    speed: 3000,
    // autoplay: true,
    fade: true,
    autoplaySpeed: 5000,
    asNavFor: '.main-slider__nav-container',
    // infinite: false,
    prevArrow: $('.main-slider__arr-prev'),
    nextArrow: $('.main-slider__arr-next')
  });
  $('.main-slider__nav-container').slick({
    speed: 2000,
    slidesToShow: 3,
    asNavFor: '.main-slider__grand',
    focusOnSelect: true,
    arrows: false,
    // infinite: false
    responsive: [{
      breakpoint: 450,
      settings: {
        slidesToShow: 2
      }
    }]
  });
  $('body').on('click', '.scrollTop', function () {
    $('html, body').animate({
      scrollTop: 0
    }, 500);
    return false;
  });
  mainSliderNavMargin(width); // accordion

  $('body').on('click', '.accordion-head', function () {
    $(this).siblings('.accordion-body').slideToggle();
  });
  $(document).mouseup(function (e) {
    // событие клика по веб-документу
    var div = $(".header-bottom__mobile-menu"); // тут указываем ID элемента

    if (!div.is(e.target) && div.has(e.target).length === 0) {
      // если клик был не по нашему блоку
      div.slideUp('open');
      $('.header-bottom__mobile').removeClass('open');
    }
  });
  $('.filter__name').click(function () {
    $(this).closest('.filter').toggleClass('open');
  });
  $(document).mouseup(function (e) {
    // событие клика по веб-документу
    var div = $(".filter__select"); // тут указываем ID элемента

    if (!div.is(e.target) // если клик был не по нашему блоку
    && div.has(e.target).length === 0) {
      // и не по его дочерним элементам
      div.closest('.filter').removeClass('open');
    }
  });
  $(document).mouseup(function (e) {
    // событие клика по веб-документу
    var div = $(".header-phones__back"); // тут указываем ID элемента

    if (!div.is(e.target) // если клик был не по нашему блоку
    && div.has(e.target).length === 0) {
      // и не по его дочерним элементам
      div.removeClass('open');
      $('.header-top__phones > .icon-cross.open').removeClass('open');
    }
  });
  $('body').on('click', '.header-phones__front,  .header-phones__back .close-mob', function () {
    $(this).addClass('open');
    $('.header-phones__back').addClass('open');
  });
  $('body').on('click', '.header-phones__front.open', function () {
    $(this).removeClass('open');
    $('.header-phones__back').removeClass('open');
  });

  if ($('.styler')) {
    $('.styler').styler();
  }

  $('[data-tab-on="on"] [data-li]').click(function () {
    var dataLi = $(this).attr('data-li');

    if (window.location.pathname == '/services/transaction_support/mediatsiya/' || window.location.pathname == '/exchange_info/count_analitics/informatsionnye-uslugi/' || window.location.pathname == '/by/services/transaction_support/mediatsiya/' || window.location.pathname == '/by/exchange_info/count_analitics/informatsionnye-uslugi/') {
      if (!$(this).hasClass('active')) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').addClass('active').siblings().removeClass('active');
        $('.timetables-market').nextAll().hide();
      } else if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').removeClass('active');
        $('.timetables-market').nextAll().show();
      }
    } else {
      if (!$(this).hasClass('active')) {
        $(this).addClass('active').siblings().removeClass('active');
        $(this).closest('[data-tab-on]').find('[data-tab=' + dataLi + ']').addClass('active').siblings().removeClass('active');
      }
    }
  });
  $('.timetables-market__filter').slick({
    slidesToShow: 3,
    centerMode: true,
    infinite: false,
    initialSlide: 1
  }); // appender

  function appender() {
    var width = $(window).width();
    var height = $(window).height();

    if (width > 1220) {
      $('.header').attr('data-mob', 'false');
      menuDex();
    } else {
      $('.header').attr('data-mob', 'true');
      menuMob();
    }
  }

  appender();
  $(window).resize(appender);

  function menuMob() {
    $('.header-bottom__container').prepend($('.header-top__phones'));
    $('.header-bottom__mobile-menu .container').append($('.header-top__btns'));
    $('.header-bottom__mobile-menu .container').append($('.header-top__tabs'));
    $('.header-top__tabs li[data-tab="1"]').append($('.header-bottom__tabs > div[data-tab="1"]'));
    $('.header-top__tabs li[data-tab="2"]').append($('.header-bottom__tabs > div[data-tab="2"]'));
    $('.header-bottom__mobile-menu .container').append($('.header-bottom__menu'));
  }

  function menuDex() {
    $('.header-top__container').prepend($('.header-top__phones'));
    $('.header-top__container').append($('.header-top__tabs'));
    $('.header-top__container').append($('.header-top__btns'));
    $('.header-bottom__tabs').append($('.header-tabs-container[data-tab="1"]'));
    $('.header-bottom__tabs').append($('.header-tabs-container[data-tab="2"]'));
    $('.header-bottom__container').append($('.header-bottom__menu'));
  } // menu mobile handler


  $('body').on('click', '.header-bottom__mobile .icon-mobile-menu', function () {
    $('.header-bottom__mobile').toggleClass('open');
    $('.header-bottom__mobile-menu').slideToggle();
  });

  if ($('.flipbook').length) {
    $(".flipbook").turn({
      width: 400,
      height: 300,
      autoCenter: true
    });
  }

  $('body').on('click', '.header-btns__item--calendar > .icon, .header-btns__item--calendar > span ', function () {
    $('.popup-calendar').toggleClass('show');
    return false;
  });
  $(document).mouseup(function (e) {
    // событие клика по веб-документу
    var div = $(".header-btns__item--calendar"); // тут указываем ID элемента

    if (!div.is(e.target) // если клик был не по нашему блоку
    && div.has(e.target).length === 0) {
      // и не по его дочерним элементам
      $('.popup-calendar').removeClass('show');
    }
  }); // КАЛЕНДАРЬ (на бою вынесен в хедер)
  // var eventData = [
  // 	{"date":"2019-12-26",'active': 'false'},
  // 	{"date":"2019-12-25"}
  // ];
  //
  // $(".butb--calendar").zabuto_calendar({
  // 	cell_border: false,
  // 	data: eventData,
  // 	show_days: false,
  // 	language: "RU",
  // 	weekstartson: 1,
  // 	today: true,
  // 	action: function () {
  //
  // 		alert(this.id.replace(/date_/g, ""))
  //
  // 	},
  // 	action_nav: function () {
  // 	},
  // 	nav_icon: {
  // 		prev: '<i class="fa fa-chevron-left"></i>',
  // 		next: '<i class="fa fa-chevron-right"></i>'
  // 	}
  // });

  mobContHeight();
  $('.ajax-popup-link').magnificPopup({
    type: 'ajax',
    callbacks: {
      ajaxContentAdded: function ajaxContentAdded() {
        if ($("#flipbook").length) {
          $("#flipbook").turn({
            width: 580,
            height: 411
          });
        }

        ;
      }
    }
  }); // function footerAccordion() {
  // 	if($(window).width() <= 450) {
  // 		$('.footer-item').addClass('accordion');
  // 		$('.footer-item-zag').addClass('accordion-head');
  // 		$('.footer-item > ul').addClass(('accordion-body'));
  // 	}
  //
  // 	else if($(Window).width() > 450) {
  // 		$('.footer-item').removeClass('accordion');
  // 		$('.footer-item-zag').removeClass('accordion-head');
  // 		$('.footer-item > ul').removeClass(('accordion-body'));
  // 	}
  // }
  // footerAccordion();
  // $(window).resize(footerAccordion);

  $('body').on('click', '.header-tabs__menu > li', function () {
    $(this).find('.dropmenu').slideToggle();
    $(this).find('.icon').toggleClass('open');
  });
  $('.img-expand').magnificPopup({
    type: 'image'
  });
  $('.popup-youtube').magnificPopup({
    disableOn: 700,
    type: 'iframe',
    mainClass: 'mfp-fade video-style',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: true
  });
  $('body').on('click', '.text-page__menu .has-drop > a', function () {
    $(this).closest('.has-drop').toggleClass('open');
    $(this).siblings('ul').slideToggle(300);
    return false;
  });
});
$(window).resize(function () {
  var width = $(window).width();
  var height = $(window).height();
  mainSliderNavMargin(width);
  mobContHeight();
});
$(window).scroll(function () {
  var width = $(window).width();
  var height = $(window).height();
});

function mainSliderNavMargin(width) {
  var offsetContainer = (width - $('.container').width()) / 2;
  var padding = parseInt($('.main-slider__nav .slick-slide').css('padding-right'));
  $('.main-slider__nav').css('margin-right', offsetContainer - padding + 'px');
} // Функция расчета высоты блока контактов


function mobContHeight() {
  if ($(window).width() <= 1220) {
    var hH = $('.header').outerHeight();
    var blockH = $(window).height() - hH;
    $('.header-phones__back').css('height', blockH + 'px');
    $('.header-bottom__mobile-menu').css('height', blockH + 'px'); // Для блока контактов
    // let contHeight = $('.header-phones__back .container').outerHeight();
    // let calcHeight = contHeight - headHeight - 20;
    //
    // // Для блока меню
    // let calcMenu = $(window).height() - headHeight - 12;
  }
}

$(document).ready(function () {
  $('.simple-ajax-popup-align-top').magnificPopup({
    type: 'ajax'
  });
});